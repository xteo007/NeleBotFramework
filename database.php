<?php

/*
NeleBotFramework
    Copyright (C) 2018  PHP-Coders

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$pluginp = $f['database'];

# Connessione a Redis
if ($config['usa_redis']) {
    // Configurazioni di Redis per l'accesso ai Database
    $redisc = $config['redis'];
    // Connessione a Redis
    try {
        $redis = new Redis();
        $redis->connect($redisc['host'], $redisc['port']);
    } catch (Exception $e) {
        call_error($e->getMessage(), $f['database']);
        die();
    }
    // Autenticazione
    if ($redisc['password'] !== false) {
        try {
            $redis->auth($redisc['password']);
        } catch (Exception $e) {
            call_error($e);
            die();
        }
    }
    // Selezione del database Redis
    if ($redisc['database'] !== false) {
        try {
            $redis->select($redisc['database']);
        } catch (Exception $e) {
            call_error($e);
            die();
        }
    }
    //Test funzionamento
    if ($cmd == "redis" and $isadmin) {
        $test = $redis->ping();
        if ($test == "+PONG") {
            $res = "Positivo";
        } else {
            $res = "Negativo";
        }
        sm($chatID, bold("Risultato del Test: ") . $res);
    }

    require($f['anti-flood']);
} else {
    if ($cmd == "redis" and $isadmin) {
        sm($chatID, "Redis è disattivato! \nAttivalo dal file config.");
    }
}

# Connessione al Database
if ($config['usa_il_db']) {
    if (strtolower($database['type']) == 'mysql') {
        try {
            $PDO = new PDO("mysql:host=" . $database['host'] . ";dbname=" . $database['nome_database'],
                $database['utente'], $database['password']);
        } catch (PDOException $e) {
            call_error($e->getMessage(), $f['database']);
            die;
        }

        $query = "CREATE TABLE IF  NOT EXISTS utenti (
		user_id BIGINT(50)  NOT NULL ,
		nome VARCHAR(50)  NOT NULL ,
		cognome VARCHAR(50)  NOT NULL ,
		username VARCHAR(50)  NOT NULL ,
		lang VARCHAR(50)  NOT NULL ,
		page VARCHAR(50)  NOT NULL ,
		status VARCHAR(50)  NOT NULL);";
        $PDO->query($query);
        $err = $PDO->errorInfo();
        if ($err[0] !== "00000") {
            call_error("PDO Error\n<b>INPUT:</> <code>$query</>\n<b>OUTPUT:</> <code>" . json_encode($err) . "</>");
            die;
        }

        $query = "CREATE TABLE IF  NOT EXISTS gruppi (
		chat_id BIGINT(50)  NOT NULL ,
		title VARCHAR(50)  NOT NULL ,
		description VARCHAR(256)  NOT NULL ,
		username VARCHAR(50)  NOT NULL ,
		admins VARCHAR(4096)  NOT NULL ,
		page VARCHAR(50)  NOT NULL ,
		status VARCHAR(50)  NOT NULL);";
        $PDO->query($query);
        $err = $PDO->errorInfo();
        if ($err[0] !== "00000") {
            call_error("PDO Error\n<b>INPUT:</> <code>$query</>\n<b>OUTPUT:</> <code>" . json_encode($err) . "</>");
            die;
        }

        if ($config['post_canali']) {
            $query = "CREATE TABLE IF  NOT EXISTS canali (
			chat_id BIGINT(50)  NOT NULL ,
			title VARCHAR(50)  NOT NULL ,
			description VARCHAR(256)  NOT NULL ,
			username VARCHAR(50)  NOT NULL ,
			admins VARCHAR(4096)  NOT NULL ,
			page VARCHAR(50)  NOT NULL ,
			status VARCHAR(50)  NOT NULL);";
            $PDO->query($query);
            $err = $PDO->errorInfo();
            if ($err[0] !== "00000") {
                call_error("PDO Error\n<b>INPUT:</> <code>$query</>\n<b>OUTPUT:</> <code>" . json_encode($err) . "</>");
                die;
            }
        }
    } elseif (strtolower($database['type']) == 'postgre') {
        try {
            $PDO = new PDO("pgsql:host=" . $database['host'] . ";dbname=" . $database['nome_database'],
                $database['utente'], $database['password']);
        } catch (PDOException $e) {
            call_error($e->getMessage(), $f['database']);
            die;
        }

        $query = "CREATE TABLE IF  NOT EXISTS utenti (
		user_id BIGINT NOT NULL ,
		nome VARCHAR NOT NULL ,
		cognome VARCHAR NOT NULL ,
		username VARCHAR NOT NULL ,
		lang VARCHAR NOT NULL ,
		page VARCHAR NOT NULL ,
		status VARCHAR NOT NULL);";
        $PDO->query($query);
        $err = $PDO->errorInfo();
        if ($err[0] !== "00000") {
            call_error("PDO Error\n<b>INPUT:</> <code>$query</>\n<b>OUTPUT:</> <code>" . json_encode($err) . "</>");
            die;
        }

        $query = "CREATE TABLE IF  NOT EXISTS gruppi (
		chat_id BIGINT NOT NULL ,
		title VARCHAR  NOT NULL ,
		description VARCHAR  NOT NULL ,
		username VARCHAR  NOT NULL ,
		admins VARCHAR  NOT NULL ,
		page VARCHAR  NOT NULL ,
		status VARCHAR  NOT NULL);";
        $PDO->query($query);
        $err = $PDO->errorInfo();
        if ($err[0] !== "00000") {
            call_error("PDO Error\n<b>INPUT:</> <code>$query</>\n<b>OUTPUT:</> <code>" . json_encode($err) . "</>");
            die;
        }

        if ($config['post_canali']) {
            $query = "CREATE TABLE IF  NOT EXISTS canali (
			chat_id BIGINT  NOT NULL ,
			title VARCHAR  NOT NULL ,
			description VARCHAR  NOT NULL ,
			username VARCHAR  NOT NULL ,
			admins VARCHAR  NOT NULL ,
			page VARCHAR  NOT NULL ,
			status VARCHAR  NOT NULL);";
            $PDO->query($query);
            $err = $PDO->errorInfo();
            if ($err[0] !== "00000") {
                call_error("PDO Error\n<b>INPUT:</> <code>$query</>\n<b>OUTPUT:</> <code>" . json_encode($err) . "</>");
                die;
            }
        }
    } else {
        call_error("Errore: tipo di database sconosciuto.", $f['database']);
        die;
    }

    $q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
    $q->execute([$userID]);
    $err = $q->errorInfo();
    if ($err[0] !== "00000") {
        call_error("PDO Error\n<b>INPUT:</> " . code(json_encode($q)) . " \n<b>OUTPUT:</> " . code(json_encode($err)));
    }

    if (isset($q) && $q) {
        $u = $q->fetch(\PDO::FETCH_ASSOC);
    } else {
        $u = false;
    }

    if (!$u and $exists_user) {
        if (!$cognome) {
            $cognome = "";
        }
        if (!$username) {
            $username = "";
        }
        if (!$lingua) {
            $lingua = "en-US";
        }
        $q = $PDO->prepare("INSERT INTO utenti (user_id, nome, cognome, username, lang, page, status) VALUES (?,?,?,?,?,?,?)");
        $q->execute([$userID, $nome, $cognome, $username, $lingua, '', 'avviato']);
        $err = $q->errorInfo();
        if ($err[0] !== "00000") {
            call_error("PDO Error\n<b>INPUT:</> " . code(json_encode($q)) . "\n<b>OUTPUT:</> " . code(json_encode($err)));
        }
        $u = db_query("SELECT * FROM utenti WHERE user_id = $userID");
    }

    if ($u and $exists_user) {
        if (!$cognome) {
            $cognome = "";
        }
        if (!$username) {
            $username = "";
        }
        if ($nome !== $u['nome'] or $cognome !== $u['cognome'] or $username !== $u['username']) {
            $e = db_query("UPDATE utenti SET nome = ?, cognome = ?, username = ? WHERE user_id = $userID",
                [$nome, $cognome, $username]);
        }
        if ($u['status'] == "inattesa") {
            db_query("UPDATE utenti SET status = 'attivo' WHERE user_id = $userID");
        }
    }

    // Datbase di Gruppi e Canali
    if ($chatID !== $userID) {

        if ($typechat == "supergroup") {
            $qg = $PDO->prepare("SELECT * FROM gruppi WHERE chat_id = ?");
            $qg->execute([$chatID]);
            $err = $qg->errorInfo();
            if ($err[0] !== "00000") {
                call_error("PDO Error\n<b>INPUT:</> " . code(json_encode($qg)) . " \n<b>OUTPUT:</> " . code(json_encode($err)));
            }

            if (isset($qg) && $qg) {
                $g = $qg->fetch(\PDO::FETCH_ASSOC);
            } else {
                $g = false;
            }

            if (!$g) {
                if (!isset($usernamechat)) {
                    $usernamechat = "";
                }
                $descrizione = getChat($chatID);
                $descrizione = $descrizione['result']['description'];
                if (!isset($descrizione)) {
                    $descrizione = "";
                }
                $adminsg = getAdmins($chatID);
                $adminsg = json_encode($adminsg['result']);
                $q = $PDO->prepare("INSERT INTO gruppi (chat_id, title, description, username, admins, page, status) VALUES (?,?,?,?,?,?,?)");
                $q->execute([$chatID, $title, $descrizione, $usernamechat, $adminsg, '', 'attivo']);
                $err = $q->errorInfo();
                if ($err[0] !== "00000") {
                    call_error("PDO Error\n<b>INPUT:</> " . code(json_encode($q)) . "\n<b>OUTPUT:</> " . code(json_encode($err)));
                }
                $g = db_query("SELECT * FROM gruppi WHERE chat_id = $userID");
            }

            if ($g) {
                if (!isset($usernamechat)) {
                    $usernamechat = "";
                }
                if ($title !== $g['title'] or $usernamechat !== $g['username']) {
                    $descrizione = getChat($chatID);
                    $descrizione = $descrizione['result']['description'];
                    if (!isset($descrizione)) {
                        $descrizione = "";
                    }
                    $adminsg = getAdmins($chatID);
                    $adminsg = json_encode($adminsg['result']);
                    $e = db_query("UPDATE gruppi SET title = ?, username = ?, admins = ?, description = ?, page = ' ' WHERE chat_id = $chatID",
                        [$title, $usernamechat, $adminsg, $descrizione]);
                }
            }
        }

        if ($typechat == "channel") {
            $qc = $PDO->prepare("SELECT * FROM canali WHERE chat_id = ?");
            $qc->execute([$chatID]);
            $err = $qc->errorInfo();
            if ($err[0] !== "00000") {
                call_error("PDO Error\n<b>INPUT:</> " . code(json_encode($qc)) . " \n<b>OUTPUT:</> " . code(json_encode($err)));
            }

            if (isset($qc) && $qc) {
                $c = $qc->fetch(\PDO::FETCH_ASSOC);
            } else {
                $c = false;
            }

            if (!$c) {
                if (!isset($usernamechat)) {
                    $usernamechat = "";
                }
                $descrizione = getChat($chatID);
                $descrizione = $descrizione['result']['description'];
                if (!isset($descrizione)) {
                    $descrizione = "";
                }
                $adminsg = getAdmins($chatID);
                $adminsg = json_encode($adminsg['result']);
                $q = $PDO->prepare("INSERT INTO canali (chat_id, title, description, username, admins, page, status) VALUES (?,?,?,?,?,?,?)");
                $q->execute([$chatID, $title, $descrizione, $usernamechat, $adminsg, '', 'attivo']);
                $err = $q->errorInfo();
                if ($err[0] !== "00000") {
                    call_error("PDO Error\n<b>INPUT:</> " . code(json_encode($qc)) . "\n<b>OUTPUT:</> " . code(json_encode($err)));
                }
                $c = db_query("SELECT * FROM canali WHERE chat_id = $userID");
            }

            if ($c) {
                if (!isset($usernamechat)) {
                    $usernamechat = "";
                }
                if ($title !== $c['title'] or $usernamechat !== $c['username']) {
                    $descrizione = getChat($chatID);
                    $descrizione = $descrizione['result']['description'];
                    if (!isset($descrizione)) {
                        $descrizione = "";
                    }
                    $adminsg = getAdmins($chatID);
                    $adminsg = json_encode($adminsg['result']);
                    db_query("UPDATE canali SET title = ?, username = ?, admins = ?, description = ?, page = ' ' WHERE chat_id = $chatID",
                        [$title, $usernamechat, $adminsg, $descrizione]);
                }
            }
        }

    }

    // Comandi sul Database solo per Amministratori del Bot
    if ($isadmin) {
        # Comando per il Ban di un utente dal Bot
        if (strpos($cmd, "ban ") === 0 and $typechat == "private") {
            $id = str_replace("@", '', str_replace("ban ", '', $cmd));
            if (isset($id)) {
                $q = $PDO->prepare("UPDATE utenti SET status = ? WHERE user_id = ? or username = ?");
                $q->execute(['ban', $id, $id]);
                sm($chatID, "Bannato l'utente " . $id . " dal Bot");
            }
        }
        # Comando per l'Unban di un utente dal Bot
        if (strpos($cmd, "unban ") === 0 and $typechat == "private") {
            $id = str_replace("@", '', str_replace("unban ", '', $cmd));
            if (isset($id)) {
                $q = $PDO->prepare("UPDATE utenti SET status = ? WHERE user_id = ? or username = ?");
                $q->execute(['inattesa', $id, $id]);
                sm($chatID, "Sbannato l'utente " . $id . " dal Bot");
            }
        }
        if ($cmd == "database") {
            if ($typechat == "supergroup") {
                sm($chatID, code(json_encode($g, JSON_PRETTY_PRINT)));
            } else {
                sm($chatID, code(json_encode($u, JSON_PRETTY_PRINT)));
            }
        }
        if (strpos($cmd, "query ") === 0) {
            $query = str_replace('query ', '', $cmd);
            $q = $PDO->prepare($query);
            $q->execute();
            $r = $q->fetch(\PDO::FETCH_ASSOC);
            $error = $PDO->errorInfo();
            sm($chatID, bold("Query: ") . code($query) . "\n" . bold("Risultato: ") . code(substr(json_encode($r), 0,
                    255)) . "\n" . bold("Errori: ") . code(substr(json_encode($error), 0, 255)));
        }
    }

    // Comando Riavvia
    if ($cmd == "riavvia") {
        db_query("UPDATE utenti SET page = '' WHERE user_id = $userID");
        if (isset($g['chat_id'])) {
            if (!isset($usernamechat)) {
                $usernamechat = "";
            }
            $descrizione = getChat($chatID);
            $descrizione = $descrizione['result']['description'];
            if (!isset($descrizione)) {
                $descrizione = "";
            }
            $adminsg = getAdmins($chatID);
            $adminsg = json_encode($adminsg['result']);
            $e = db_query("UPDATE gruppi SET title = ?, username = ?, admins = ?, description = ? WHERE chat_id = $chatID",
                [$title, $usernamechat, $adminsg, $descrizione]);
        }
        if (isset($c['chat_id'])) {
            if (!isset($usernamechat)) {
                $usernamechat = "";
            }
            $descrizione = getChat($chatID);
            $descrizione = $descrizione['result']['description'];
            if (!isset($descrizione)) {
                $descrizione = "";
            }
            $adminsg = getAdmins($chatID);
            $adminsg = json_encode($adminsg['result']);
            $e = db_query("UPDATE canali SET title = ?, username = ?, admins = ?, description = ? WHERE chat_id = $chatID",
                [$title, $usernamechat, $adminsg, $descrizione]);
        }
        sm($chatID, "Bot riavviato");
        die;
    }

} else {
    if ($cmd == "database" and $isadmin) {
        sm($chatID, "Database non attivo");
    }
}