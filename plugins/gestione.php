<?php

/*
NeleBotFramework
    Copyright (C) 2018  PHP-Coders

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if ($config['usa_il_db']) {

    function progressbar($now, $tot)
    {
        $p = ($now / $tot) * 100;
        if ($p <= 10) {
            $b = "🕑";
        } elseif ($p >= 11 && $p <= 20) {
            $b = "🕒";
        } elseif ($p >= 21 && $p <= 30) {
            $b = "🕓";
        } elseif ($p >= 31 && $p <= 40) {
            $b = "🕔";
        } elseif ($p >= 41 && $p <= 50) {
            $b = "🕕";
        } elseif ($p >= 51 && $p <= 60) {
            $b = "🕖";
        } elseif ($p >= 61 && $p <= 70) {
            $b = "🕗";
        } elseif ($p >= 71 && $p <= 80) {
            $b = "🕘";
        } elseif ($p >= 81 && $p <= 90) {
            $b = "🕙";
        } elseif ($p >= 91 && $p <= 100) {
            $b = "🕛";
        } else {
            $b = "?";
        }
        return $b;
    }

    if ($cmd == "system" and $isadmin) {
        $testo = bold("Statistiche di sistema 🗄");
        $disco_libero = disk_free_space("/");
        $disco_totale = disk_total_space("/");
        $disco_utilizzato = $disco_totale - $disco_libero;
        $testo .= "\n\nUtilizzo disco: " . round($disco_utilizzato / 1024 / 1024) . " su " . round($disco_totale / 1024 / 1024) . " MB (" . round($disco_utilizzato / $disco_totale * 100) . "%)";
        $ram_utilizzata = memory_get_usage(true);
        $ram_totale = $phpinfo['PHP Core']['memory_limit'];
        if ($ram_totale) {
            $testo .= "\nUtilizzo RAM: " . round($ram_utilizzata / 1024 / 1024) . " su " . round($ram_totale / 1024 / 1024) . " MB (" . round($ram_utilizzato / $ram_totale * 100) . "%)";
        } else {
            $testo .= "\nUtilizzo RAM: " . round($ram_utilizzata / 1024 / 1024, 1) . " MB";
        }
        $cpu = sys_getloadavg();
        $testo .= "\nLoad: " . json_encode($cpu);
        sm($chatID, $testo);
    }

    if ($cmd == "gestione" and $isadmin or $cbdata == "/gestione" and $isadmin) {
        $menu[0] = array(
            array(
                'text' => "Utenti 👤",
                'callback_data' => '/gestione utenti 1'
            ),
        );
        $menu[1] = array(
            array(
                'text' => "Gruppi 👥",
                'callback_data' => '/gestione gruppi 1'
            ),
            array(
                'text' => "Canali 📢",
                'callback_data' => '/gestione canali 1'
            ),
        );
        $menu[2] = array(
            array(
                'text' => "Fatto ✅",
                'callback_data' => 'fatto'
            ),
        );
        $testo = "Cosa vuoi gestire?";
        if ($cbdata) {
            cb_reply($cbid, '', false, $cbmid, $testo, $menu);
        } else {
            sm($chatID, $testo, $menu);
        }
    }

    if (strpos($cbdata, "/gestione ") === 0 and $isadmin) {
        $idb = ['utenti', 'gruppi', 'canali'];
        $e = explode(" ", $cbdata);
        $db = $e[1];
        $page = $e[2];
        if (in_array($db, $idb)) {
            $cosi = db_query("SELECT * FROM $db");
            $ultimo = (5 * $page) - 1;
            $primo = $ultimo - 4;
            $range = range($primo, $ultimo);
            foreach ($range as $num) {
                $coso = $cosi[$num];
                $numero = $num + 1;
                if ($numero <= 9) {
                    $numero .= "️⃣";
                }
                if (isset($coso['user_id'])) {
                    if ($cognome) {
                        $coso['nome'] .= " " . $coso['cognome'];
                    }
                    $testo .= "\n$numero - " . textspecialchars($coso['nome']) . " [" . code($coso['user_id']) . "]";
                    $leggenda = italic("🔄 Aggiorna informazioni \nℹ️ Informazioni utente \n🗑 Elimina l'utente \n🚷 Banna utente");
                    $menu[] = array(
                        array(
                            "text" => $numero,
                            "callback_data" => "nothing"
                        ),
                        array(
                            "text" => "🔄",
                            "callback_data" => "/updateuser " . $coso['user_id']
                        ),
                        array(
                            "text" => "ℹ️",
                            "callback_data" => "/infouser " . $coso['user_id']
                        ),
                        array(
                            "text" => "🗑",
                            "callback_data" => "/deluser " . $coso['user_id']
                        ),
                        array(
                            "text" => "🚷",
                            "callback_data" => "/banuser " . $coso['user_id']
                        ),
                    );
                } elseif (isset($coso['chat_id'])) {
                    $leggenda = italic("🔄 Aggiorna informazioni \nℹ️ Informazioni sulla chat \n🚮 Lascia la chat \n🗑 Elimina la chat \n🚷 Banna chat");
                    $menu[] = array(
                        array(
                            "text" => $numero,
                            "callback_data" => "nothing"
                        ),
                        array(
                            "text" => "🔄",
                            "callback_data" => "/updatechat " . $coso['chat_id']
                        ),
                        array(
                            "text" => "ℹ️",
                            "callback_data" => "/infochat " . $coso['chat_id']
                        ),
                        array(
                            "text" => "🚮",
                            "callback_data" => "/leavechat " . $coso['chat_id']
                        ),
                        array(
                            "text" => "🗑",
                            "callback_data" => "/delchat " . $coso['chat_id']
                        ),
                        array(
                            "text" => "🚷",
                            "callback_data" => "/banchat " . $coso['chat_id']
                        ),
                    );
                    $testo .= "\n$numero - " . textspecialchars($coso['title']) . " [" . code($coso['chat_id']) . "]";
                }
            }
            if ($page !== "1") {
                $menufrecce[] = array(
                    "text" => "⏪",
                    "callback_data" => "/gestione $db " . ($page - 1)
                );
            }
            if (isset($cosi[$ultimo])) {
                $menufrecce[] = array(
                    "text" => "⏩",
                    "callback_data" => "/gestione $db " . ($page + 1)
                );
            }
            if (isset($menufrecce)) {
                $menu[] = $menufrecce;
            }
            $menu[] = array(
                array(
                    "text" => "Indietro",
                    "callback_data" => "/gestione"
                ),
            );
            cb_reply($cbid, '', false, $cbmid, bold("Gestione $db \n") . $testo . "\n\n$leggenda", $menu);
        } else {
            cb_reply($cbid, "Errore: database sconosciuto");
        }
    }

    if (strpos($cbdata, "/updateuser ") === 0 and $isadmin) {
        $user_ID = str_replace('/updateuser ', '', $cbdata);
        $user = getChat($user_ID);
        if ($chat['ok'] === false) {
            cb_reply($cbid, "Non sono riuscito a ricevere le sue info!", true);
            die;
        }
        $user = $user['result'];
        if (!$user['last_name']) {
            $user['last_name'] = "";
        }
        if (!$user['username']) {
            $user['username'] = "";
        }
        db_query("UPDATE utenti SET nome = ?, cognome = ?, username = ? WHERE user_id = $user_ID",
            [$user['first_name'], $user['last_name'], $user['username']]);
        cb_reply($cbid, "Aggiornato", false);

    }

    if (strpos($cbdata, "/updatechat ") === 0 and $isadmin) {
        $chat_ID = str_replace('/updatechat ', '', $cbdata);
        $chat = getChat($chat_ID);
        if ($chat['ok'] === false) {
            cb_reply($cbid, "Non sono riuscito a ricevere le informazioni di questa chat!", true);
            die;
        }
        $chat = $chat['result'];
        $title = $chat['title'];
        $descrizione = $chat['description'];
        if (!isset($descrizione)) {
            $descrizione = "";
        }
        $adminsg = getAdmins($chat_ID);
        $adminsg = json_encode($adminsg['result']);
        db_query("UPDATE canali SET title = ?, username = ?, admins = ?, description = ? WHERE chat_id = $chat_ID",
            [$title, $usernamechat, $adminsg, $descrizione]);
        db_query("UPDATE gruppi SET title = ?, username = ?, admins = ?, description = ? WHERE chat_id = $chat_ID",
            [$title, $usernamechat, $adminsg, $descrizione]);
        cb_reply($cbid, "Aggiornato", false);
    }

    if (strpos($cbdata, "/infouser ") === 0 and $isadmin) {
        $id = str_replace('/infouser ', '', $cbdata);
        $q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
        $q->execute([$id]);
        $user = $q->fetch(\PDO::FETCH_ASSOC);
        $testo = "Nome: " . $user['nome'];
        if ($user['cognome']) {
            $testo .= " " . $user['cognome'];
        }
        if ($user['username']) {
            $testo .= " \nUsername: @" . $user['username'];
        }
        $testo .= " \nPage: '" . $user['page'] . "' \n";
        if ($user['status'] == "ban") {
            $testo .= "Stato: Bannato";
        } else {
            $testo .= "Stato: Libero";
        }
        cb_reply($cbid, "Informazioni Utente \n$testo", true);
    }

    if (strpos($cbdata, "/infochat ") === 0 and $isadmin) {
        $id = str_replace('/infochat ', '', $cbdata);
        $q = $PDO->prepare("SELECT * FROM canali WHERE chat_id = ?");
        $q->execute([$id]);
        $chat = $q->fetch(\PDO::FETCH_ASSOC);
        if (!isset($chat['chat_id'])) {
            $q = $PDO->prepare("SELECT * FROM gruppi WHERE chat_id = ?");
            $q->execute([$id]);
            $chat = $q->fetch(\PDO::FETCH_ASSOC);
            if (!isset($chat['chat_id'])) {
                cb_reply($cbid, "Chat non trovata nel database", true);
                die;
            }
        }
        $testo = "Titolo: " . $chat['title'];
        if ($chat['username']) {
            $testo .= " \nUsername: @" . $chat['username'];
        }
        $admi = json_decode($chat['admins'], true);
        if (isset($admi['result'])) {
            $admi = $admi['result'];
        }
        foreach ($admi as $adminsa) {
            if ($adminsa['status'] == 'creator') {
                $founder = $adminsa['user']['first_name'] . " [" . $adminsa['user']['id'] . "]";
            }
        }
        $testo .= "\nFounder: " . $founder;
        $testo .= " \nPage: '" . $chat['page'] . "' \n";
        if ($chat['status'] == "ban") {
            $testo .= "Stato: Bannato";
        } elseif ($chat['status'] == "inattivo") {
            $testo .= "Stato: bloccato";
        } elseif ($chat['status'] == "inattesa") {
            $testo .= "Stato: sbannato";
        } else {
            $testo .= "Stato: Libero";
        }
        cb_reply($cbid, "Informazioni Chat \n$testo", true);
    }

    if (strpos($cbdata, "/banchat ") === 0 and $isadmin) {
        $id = str_replace('/banchat ', '', $cbdata);
        $q = $PDO->prepare("SELECT * FROM canali WHERE chat_id = ?");
        $q->execute([$id]);
        $chat = $q->fetch(\PDO::FETCH_ASSOC);
        $db = "canali";
        if (!isset($chat['chat_id'])) {
            $q = $PDO->prepare("SELECT * FROM gruppi WHERE chat_id = ?");
            $q->execute([$id]);
            $chat = $q->fetch(\PDO::FETCH_ASSOC);
            $db = "gruppi";
            if (!isset($chat['chat_id'])) {
                cb_reply($cbid, "Chat non trovata nel database", true);
                die;
            }
        }
        if ($chat['status'] == "ban") {
            $ban = "sbannata";
            db_query("UPDATE $db SET status = 'wait' WHERE chat_id = $id");
        } else {
            $ban = "bannata";
            db_query("UPDATE $db SET status = 'ban' WHERE chat_id = $id");
        }
        cb_reply($cbid, "Chat $ban dai $db", true);
    }

    if (strpos($cbdata, "/banuser ") === 0 and $isadmin) {
        $id = str_replace('/banuser ', '', $cbdata);
        $q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
        $q->execute([$id]);
        $user = $q->fetch(\PDO::FETCH_ASSOC);
        if ($user['status'] == "ban") {
            $ban = "sbannato";
            db_query("UPDATE utenti SET status = 'wait' WHERE user_id = $id");
        } else {
            $ban = "bannato";
            db_query("UPDATE utenti SET status = 'ban' WHERE user_id = $id");
        }
        cb_reply($cbid, "Utente $ban", true);
    }

    if (strpos($cbdata, "/leavechat ") === 0 and $isadmin) {
        $id = str_replace('/leavechat ', '', $cbdata);
        $q = $PDO->prepare("SELECT * FROM canali WHERE chat_id = ?");
        $q->execute([$id]);
        $chat = $q->fetch(\PDO::FETCH_ASSOC);
        $db = "canali";
        if (!isset($chat['chat_id'])) {
            $q = $PDO->prepare("SELECT * FROM gruppi WHERE chat_id = ?");
            $q->execute([$id]);
            $chat = $q->fetch(\PDO::FETCH_ASSOC);
            $db = "gruppi";
            if (!isset($chat['chat_id'])) {
                cb_reply($cbid, "Chat non trovata nel database", true);
                die;
            }
        }
        lc($chat['chat_id']);
        cb_reply($cbid, "Ho abbandonato questa chat", true);
    }

    if (strpos($cbdata, "/deluser ") === 0 and $isadmin) {
        $id = str_replace('/deluser ', '', $cbdata);
        $q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
        $q->execute([$id]);
        $user = $q->fetch(\PDO::FETCH_ASSOC);
        $db = "canali";
        if (!isset($user['user_id'])) {
            cb_reply($cbid, "Utente non trovato nel database", true);
            die;
        }
        db_query("DELETE FROM utenti WHERE user_id = " . $user['user_id']);
        cb_reply($cbid, "Ho eliminato questo utente dal database", true);
    }

    if (strpos($cbdata, "/delchat ") === 0 and $isadmin) {
        $id = str_replace('/delchat ', '', $cbdata);
        $q = $PDO->prepare("SELECT * FROM canali WHERE chat_id = ?");
        $q->execute([$id]);
        $chat = $q->fetch(\PDO::FETCH_ASSOC);
        $db = "canali";
        if (!isset($chat['chat_id'])) {
            $q = $PDO->prepare("SELECT * FROM gruppi WHERE chat_id = ?");
            $q->execute([$id]);
            $chat = $q->fetch(\PDO::FETCH_ASSOC);
            $db = "gruppi";
            if (!isset($chat['chat_id'])) {
                cb_reply($cbid, "Chat non trovata nel database", true);
                die;
            }
        }
        db_query("DELETE FROM $db WHERE chat_id = " . $chat['chat_id']);
        cb_reply($cbid, "Ho eliminato questa chat dal database", true);
    }

    if ($cmd == "post" and $isadmin and $typechat == "private") {
        db_query("UPDATE utenti SET page = 'post []' WHERE user_id = $userID");
        $menu[0] = array(
            array(
                "text" => "👤Utenti ❎",
                "callback_data" => "/inviare_su utenti"
            ),
            array(
                "text" => "👥Gruppi ❎",
                "callback_data" => "/inviare_su gruppi"
            ),
        );
        if ($config['post_canali']) {
            $menu[] = array(
                array(
                    "text" => "📢Canali ❎",
                    "callback_data" => "/inviare_su canali"
                ),
            );
        }
        $menu[] = array(
            array(
                "text" => "Silenzioso 🔈",
                "callback_data" => "/inviare_su silenzioso"
            ),
        );
        sm($chatID, "📟 Seleziona dove inviare il post", $menu);
    }

    if (strpos($cbdata, "/inviare_su ") === 0 and $isadmin) {
        $array = str_replace('post ', '', $u['page']);
        $inviare = array_values(json_decode($array, true));
        if (!is_array($inviare) or !$inviare) {
            $inviare = array();
        }
        $agg = str_replace('/inviare_su ', '', $cbdata);
        if ($agg == 'utenti') {
            if (in_array('utenti', $inviare)) {
                $inviare = array_diff($inviare, ['utenti']);
            } else {
                $inviare[] = "utenti";
            }
        }
        if ($agg == 'gruppi') {
            if (in_array('gruppi', $inviare)) {
                $inviare = array_diff($inviare, ['gruppi']);
            } else {
                $inviare[] = "gruppi";
            }
        }
        if ($agg == 'silenzioso') {
            if (in_array('silenzioso', $inviare)) {
                $inviare = array_diff($inviare, ['silenzioso']);
            } else {
                $inviare[] = "silenzioso";
            }
        }
        if ($config['post_canali'] and $agg == 'canali') {
            if (in_array('canali', $inviare)) {
                $inviare = array_diff($inviare, ['canali']);
            } else {
                $inviare[] = "canali";
            }
        }
        if (in_array('utenti', $inviare)) {
            $emojiu = "✅";
        } else {
            $emojiu = "❎";
        }
        if (in_array('gruppi', $inviare)) {
            $emojig = "✅";
        } else {
            $emojig = "❎";
        }
        if (in_array('canali', $inviare)) {
            $emojic = "✅";
        } else {
            $emojic = "❎";
        }
        if (in_array('silenzioso', $inviare)) {
            $emojis = "🔇";
        } else {
            $emojis = "🔈";
        }
        $menu[0] = array(
            array(
                "text" => "👤Utenti " . $emojiu,
                "callback_data" => "/inviare_su utenti"
            ),
            array(
                "text" => "👥Gruppi " . $emojig,
                "callback_data" => "/inviare_su gruppi"
            ),
        );
        if ($config['post_canali']) {
            $menu[] = array(
                array(
                    "text" => "📢Canali " . $emojic,
                    "callback_data" => "/inviare_su canali"
                ),
            );
        }
        $menu[] = array(
            array(
                "text" => "Silenzioso " . $emojis,
                "callback_data" => "/inviare_su silenzioso"
            ),
        );
        $json = json_encode($inviare);
        db_query("UPDATE utenti SET page = 'post $json' WHERE user_id = $userID");
        cb_reply($cbid, json_encode($inviare), false, $cbmid,
            "📟 Seleziona dove inviare il post \n" . italic("Dopo la selezione inviami il post per inviarlo."), $menu);
    }

    if (strpos($u['page'], "post ") === 0 and !$cmd and !$cbdata and $typechat == "private" and $isadmin) {
        db_query("UPDATE utenti SET page = '' WHERE user_id = $userID");
        $types = json_decode(str_replace('post ', '', $u['page']), true);
        $users = [];
        if (in_array('silenzioso', $types)) {
            $config['disabilita_notifica'] = true;
            $types = array_diff($types, ['silenzioso']);
            $silenziato = " silenzioso";
        }
        foreach ($types as $db) {
            $chats = db_query("SELECT * FROM $db");
            $users = array_merge_recursive($users, $chats);
        }
        $config['response'] = true;
        if (isset($msg)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sm($chat, italic("Messaggio$silenziato per " . $title) . "\n\n" . $msg);
            }
        } elseif (isset($foto)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sp($chat, $foto, $caption);
            }
        } elseif (isset($sticker)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                ss($chat, $sticker);
            }
        } elseif (isset($gif)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sgif($chat, $gif, $caption);
            }
        } elseif (isset($video)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sv($chat, $video, $caption);
            }
        } elseif (isset($video_note)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                svr($chat, $video_note);
            }
        } elseif (isset($vocale)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sav($chat, $vocale, $caption);
            }
        } elseif (isset($audio)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sa($chat, $audio, $caption);
            }
        } elseif (isset($file)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sd($chat, $file, $caption);
            }
        } elseif (isset($venue)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sven($chat, $posizione['latitude'], $posizione['longitude'], $posto, $address);
            }
        } elseif (isset($posizione)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sendLocation($chat, $posizione['latitude'], $posizione['longitude']);
            }
        } elseif (isset($contact)) {
            sm($chatID, "Invio il post$silenziato...");
            $time_start = microtime(true);
            foreach ($users as $user) {
                if (isset($user['user_id'])) {
                    $chat = $user['user_id'];
                    $title = $user['nome'] . " " . $user['cognome'];
                } else {
                    $chat = $user['chat_id'];
                    $title = $user['title'];
                }
                sc($chat, $contact, $cnome, $ccognome);
            }
        } else {
            sm($chatID, "Non sono riuscito a riconoscere il contenuto che vuoi inviare.");
            die;
        }
        $time_now = microtime(true);
        $time_tot = round($time_now - $time_start, 2);
        sm($chatID, "Finito di inviare il tuo post! \n" . bold("Tempo impiegato: ") . $time_tot);
    }

    if ($cmd == "iscritti" or $cbdata == 'iscritti') {
        if ($isadmin) {
            $menu[0] = array(
                array(
                    'text' => "Più informazioni ➕",
                    'callback_data' => '/controllo_iscritti'
                ),
            );
            $menu[1] = array(
                array(
                    'text' => "Controllo Inattivi 🔄",
                    'callback_data' => '/controllo_inattivi'
                ),
            );
        }
        $nope = ["ban", "inattivo", "inattesa"];
        $substot = db_query("SELECT * FROM utenti");
        $subs = db_query("SELECT * FROM utenti WHERE status NOT IN ('" . implode('\',\'', $nope) . "')");
        $gruppitot = db_query("SELECT * FROM gruppi");
        $gruppi = db_query("SELECT * FROM gruppi WHERE status NOT IN ('" . implode('\',\'', $nope) . "')");
        if ($config['post_canali']) {
            $canalitot = db_query("SELECT * FROM canali");
            $canali = db_query("SELECT * FROM canali WHERE status NOT IN ('" . implode('\',\'', $nope) . "')");
            $canali = "\n" . bold("📢Canali: ") . count($canali) . "/" . count($canalitot);
        }
        $testo = bold("ISCRITTI 👥") . "\n" . bold("👤Utenti: ") . count($subs) . "/" . count($substot) . "\n" . bold("👥Gruppi: ") . count($gruppi) . "/" . count($gruppitot) . $canali;
        if ($cbdata) {
            cb_reply($cbid, '', false, $cbmid, $testo, $menu);
        } else {
            sm($chatID, $testo, $menu);
        }
    }

    if ($cbdata == "/controllo_inattivi" and $isadmin) {
        $menuc[0] = array(
            array(
                'text' => "❌ Annulla ❌",
                'callback_data' => 'ferma'
            ),
        );
        cb_reply($cbid, 'Attendi', false, $cbmid,
            bold("Controllo...🕔") . italic("\nAttendi, questa operazione puó durare più di un minuto.") . "\n",
            $menuc);
        $dbs = ["utenti", "gruppi"];
        if ($config['post_canali']) {
            $dbs[] = "canali";
        }
        $nope = ["inattivo", "ban"];
        $config['response'] = true;
        $config['disabilita_notifica'] = true;
        foreach ($dbs as $db) {
            $r = db_query("SELECT * FROM $db WHERE status NOT IN ('" . implode('\',\'', $nope) . "')");
            $tot = count($r);
            foreach ($r as $chat) {
                if (!file_exists('ferma')) {
                    if ($db == 'utenti') {
                        $type = "user_id";
                    } else {
                        $type = "chat_id";
                    }
                    $chat = $chat[$type];
                    $m = sm($chat, "Messaggio temporaneo⏱");
                    if ($m['ok'] === false) {
                        $inattivo[] = $chat;
                        db_query("UPDATE $db SET status = 'inattivo' WHERE $type = $chat");
                    } else {
                        dm($chat, $m['result']['message_id']);
                        db_query("UPDATE $db SET status = 'attivo' WHERE $type = $chat");
                    }
                    $contati[] = $chat;
                    $num = count($contati);
                    if ($thistime + 1 < time()) {
                        $thistime = time();
                        editMsg($chatID,
                            bold("Fase di Completamento di $db inattivi ") . round($num / $tot * 100) . "%" . progressbar($num,
                                $tot), $cbmid, $menuc);
                    }
                } else {
                    $annullato = true;
                }
            }
        }
        $menu[0] = array(
            array(
                'text' => "Indietro",
                'callback_data' => '/iscritti'
            ),
        );
        if ($annullato) {
            editMsg($chatID, "Operazione annullata", $cbmid, $menu);
            unlink("ferma");
        } else {
            editMsg($chatID, bold("Finito il controllo!") . "\nChat inattivi trovate sui database: " . count($inattivo),
                $cbmid, $menu);
        }
    }

    if ($cbdata == "ferma" and $isadmin) {
        file_put_contents("ferma", 'fermati');
    }

    if ($cbdata == "/controllo_iscritti" and $isadmin) {
        cb_reply($cbid, 'Attendi', false, $cbmid, bold("Controllo il database...🕔"));
        $menu[0] = array(
            array(
                'text' => "Indietro",
                'callback_data' => 'iscritti'
            ),
        );
        $dbs = ["utenti", "gruppi"];
        if ($config['post_canali']) {
            $dbs[] = "canali";
        }
        foreach ($dbs as $db) {
            $r = db_query("SELECT * FROM $db");
            unset($chats);
            foreach ($r as $chat) {
                if ($db == 'utenti') {
                    $type = "user_id";
                } else {
                    $type = "chat_id";
                }
                $stato = $chat['status'];
                $chat = $chat[$type];
                $types[$db][$stato][] = $chat;
            }
            foreach ($types[$db] as $type => $c) {
                $chats .= "\n$type: " . count($c);
            }
            $db[0] = strtoupper($db[0]);
            $testo .= "\n\n" . bold($db) . " (" . count($r) . ")" . $chats;
        }
        editMsg($chatID, bold("DATABASE ISCRITTI 👥") . $testo, $cbmid, $menu);
    }

} else {
    call_error("Database non disponibile, disattiva questo plugin.");
}