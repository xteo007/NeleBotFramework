<?php

/*
NeleBotFramework
    Copyright (C) 2018  PHP-Coders

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if ($cmd == 'start') {
    sm($chatID, "Bot avviato! \nUsa /help per una lista di Comandi.");
}

if ($cmd == 'buttons') {
    $menu[0] = array(
        array(
            'text' => "Mostra",
            'callback_data' => '/mostra'
        ),
    );
    $menu[1] = array(
        array(
            'text' => "Inline",
            'switch_inline_query_current_chat' => 'messaggio'
        ),
        array(
            'text' => "Condividi Inline",
            'switch_inline_query' => 'messaggio'
        ),
    );
    $menu[2] = array(
        array(
            'text' => "URL",
            'url' => 'https://t.me/' . $config['username_bot']
        ),
        array(
            'text' => "Condividi URL",
            'url' => 'https://t.me/share/url?' . http_build_query(array(
                    "text" => "Condiviso",
                    "url" => "https://t.me/" . $config['username_bot']
                ))
        ),
    );
    sm($chatID, "Bottoni", $menu);
}

if ($cbdata == '/mostra') {
    $cb = cb_reply($cbid, 'Ciao', true);
}

if ($cmd == 'help' and $typechat == "private") {
    $menu[0] = array(
        array(
            'text' => "Funzioni Inline",
            'switch_inline_query_current_chat' => ' '
        ),
    );
    $menu[1] = array(
        array(
            'text' => "Source Bot",
            'url' => 't.me/NelePHPFramework'
        ),
    );
    sm($chatID, bold("Comandi del Bot") . "
start - Avvia il Bot
help - Ottieni la lista dei comandi
performance - Ottieni la velocità del Bot
jsondump - Ottieni un dump in json della tua update
reply - Risponde a te e ti chiede di rispondere
buttons - Esempio di pulsanti inline
html - Ottieni lo stile di formazione in HTML
markdown - Ottieni lo stile di formattazione in Markdown
format - Formatta un messaggio
propic - Ottieni i file_id della tua ultima foto
contact - Invia un contatto
venue - Invia la posizione di un posto
location - Invia una posizione in live
@" . $config['username_bot'], $menu);
}

if ($cmd == "ping") {
    $time_start = microtime(true);
    sm($chatID, code("Pong"), false, 'def', $msgID);
    $time_end = microtime(true);
    $execution_time = round($time_end - $time_start, 3);
    sm($chatID, bold("Performance del Bot 📶 \n") . "SendMessage: $execution_time secondi");
}

if ($cmd == "reply") {
    sm($chatID, "Risposta. \nAdesso rispondi tu.", 'rispondimi', '', $msgID);
}

if ($cmd == 'jsondump') {
    sm($chatID, code(json_encode($update, JSON_PRETTY_PRINT)));
}

if ($cmd == 'html') {
    $testo = htmlspecialchars('<b>Bold</>') . " = <b>Bold</>\n";
    $testo .= htmlspecialchars('<strong>Bold</>') . " = <strong>Bold</>\n";
    $testo .= htmlspecialchars('<i>Italic</>') . " = <i>Italic</>\n";
    $testo .= htmlspecialchars('<em>Italic</>') . " = <em>Italic</>\n";
    $testo .= htmlspecialchars('<code>Fixed</>') . " = <code>Fixed</>\n";
    $testo .= htmlspecialchars('<pre>Pre-Fixed</>') . " = <pre>Pre-Fixed</>\n";
    $testo .= htmlspecialchars("<a href='http://www.example.com'>Inline URL</>") . " = <a href='http://www.example.com'>Inline URL</>\n";
    sm($chatID, $testo, false, 'html');
}

if ($cmd == 'markdown') {
    $testo = mdspecialchars('*Bold*') . " = *Bold*\n";
    $testo .= mdspecialchars('_Italic_') . " = _Italic_\n";
    $testo .= mdspecialchars('`Fixed`') . " = `Fixed`\n";
    $testo .= mdspecialchars('```Pre-Fixed```') . " = ```Pre-Fixed```\n";
    $testo .= mdspecialchars('[Inline URL](http://www.example.com)') . " = [Inline URL](http://www.example.com)";
    sm($chatID, $testo, false, 'markdown');
}

if ($cmd == 'propic') {
    $config['response'] = true;
    $photos = getPropic($userID);
    sm($chatID, "Foto Profilo Corrente: \n" . code(json_encode($photos['result']['photos'][0], JSON_PRETTY_PRINT)));
}

if ($cmd == 'contact') {
    sc($chatID, "+13253080294", "Presidente", "VoIP");
}

if ($cmd == 'editPhoto') {
    $config['response'] = true;
    $mes = sp($chatID, "t.me/NeleB54Gold", 'cia');
    sleep(2);
    editMedia($chatID, $mes['result']['message_id'], "t.me/NelePHPFramework", 'photo', 'ciao');
}

if ($cmd == 'animation') {
    sgif($chatID, "https://t.me/NeleB54GoldBlog/568", 'Rotola');
}

if ($cmd == 'venue') {
    sven($chatID, 41.90, 12.50, "Capitale d'Italia", 'Roma');
}

if ($cmd == 'location') {
    $menu[0] = array(
        array(
            'text' => "Cambia Location",
            'callback_data' => '/editLocation 42.90 13.50'
        ),
    );
    $menu[1] = array(
        array(
            'text' => "Ferma Location",
            'callback_data' => '/stopLocation'
        ),
    );
    sendLocation($chatID, 41.90, 12.50, $menu, 7200);
}

if (strpos($cbdata, '/editLocation') === 0) {
    $e = explode(' ', $cbdata, 3);
    $lati = $e[1];
    $long = $e[2];
    $latisin = $lati + 1;
    $latides = $lati - 1;
    $longsu = $long + 1;
    $longgiu = $long - 1;
    if ($longsu < 90 or $latides < 90) {
        $menu[0] = array(
            array(
                'text' => "Cambia Location",
                'callback_data' => "/editLocation $longsu $latides"
            ),
        );
    }
    $menu[] = array(
        array(
            'text' => "Ferma Location",
            'callback_data' => '/stopLocation'
        ),
    );
    editLocation($chatID, $lati, $long, $cbmid, $menu);
}

if ($cbdata == '/stopLocation') {
    $menu[0] = array(
        array(
            'text' => "Developer",
            'url' => "t.me/NeleB54Gold"
        ),
    );
    cb_reply($cbid, 'Posizionamento terminato', true);
    stopLocation($chatID, $cbmid, $menu);
}