<?php

/*
NeleBotFramework
    Copyright (C) 2018  PHP-Coders

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$config['json_payload'] = false;

if ($typechat == 'supergroup' or $typechat == 'group') {
    #Qui ci sono tutti i comandi per i Gruppi (CBData non funziona qui)

    #Permessi dello Staff di un Gruppo
    $admi = json_decode($g['admins'], true);
    if (isset($admi['result'])) {
        $admi = $admi['result'];
    }
    if (isset($g['chat_id']) and !is_array($admi)) {
        if (!isset($usernamechat)) {
            $usernamechat = "";
        }
        if ($title !== $g['title'] or $usernamechat !== $g['username']) {
            $descrizione = getChat($chatID);
            $descrizione = $descrizione['result']['description'];
            if (!isset($descrizione)) {
                $descrizione = "";
            }
            $admi = getAdmins($chatID);
            $admis = json_encode($admi['result']);
            $e = db_query("UPDATE gruppi SET title = ?, username = ?, admins = ?, description = ? WHERE chat_id = $chatID",
                [$title, $usernamechat, $admis, $descrizione]);
        }
    }
    foreach ($admi as $adminsa) {
        if ($adminsa['user']['id'] == $userID) {
            $isStaff = true;
        }

        if ($adminsa['user']['id'] == $ruserID) {
            $isrStaff = true;
        }

        if ($adminsa['user']['username'] == $config['username_bot']) {
            $botisadmin = true;
            $botperms = $adminsa;
        }

        if ($adminsa['user']['id'] == $userID and $adminsa['status'] == 'creator') {
            $isfounder = true;
        }
    }

    #Quando un utente o il Bot viene aggiunto al Gruppo
    if ($update["message"]["new_chat_member"]) {
        $nomeag = $update["message"]["new_chat_member"]["first_name"];
        $cognomeag = $update["message"]["new_chat_member"]["last_name"];
        $usernameag = $update["message"]["new_chat_member"]["username"];
        $idag = $update["message"]["new_chat_member"]["id"];
        if ($usernameag == $config['username_bot']) {
            sm($chatID,
                bold("Ciao!") . "\nSono un Bot di Test di " . text_link("NelePHPFramework", 't.me/NelePHPFramework'));
            if ($config['console'] !== false) {
                $text = "#Aggiunto \n" . bold("$nome") . " ha aggiunto @" . $config['username_bot'] . " in " . bold($title);
                if ($usernamechat) {
                    $text .= "\n" . bold("Chat:") . " $title (@$usernamechat)";
                } else {
                    $text .= "\n" . bold("Chat:") . " $title";
                }
                $text .= "\n" . bold("ID:") . " " . code($chatID);
                $text .= "\n" . bold("Utente:") . " " . textspecialchars("$nome $cognome") . " [" . code($userID) . "]";
                if ($username) {
                    $text .= "\n" . bold("Username:") . " @$username";
                }
                $text .= " \n" . bold("Bot:") . " @$usernameag";
                sm($config['console'], $text);
            }
            exit;
        }
        if ($idag == $userID) {
            sm($chatID, textspecialchars("$nomeag $cognomeag") . " è entrato.");
        } else {
            sm($chatID,
                textspecialchars("$nomeag $cognomeag") . " è stato aggiunto da " . textspecialchars("$nome $cognome"));
        }
    }

    #Quando un utente o il Bot viene rimosso dal Gruppo
    if ($update["message"]["left_chat_member"]) {
        $nomeag = $update["message"]["left_chat_member"]["first_name"];
        $cognomeag = $update["message"]["left_chat_member"]["last_name"];
        $usernameag = $update["message"]["left_chat_member"]["username"];
        $idag = $update["message"]["left_chat_member"]["id"];
        if ($usernameag == $config['username_bot']) {
            if ($config['console'] !== false) {
                $text = "#Rimosso \n" . bold($nome) . " ha rimosso @" . $config['username_bot'] . " da " . bold($title);
                if ($usernamechat) {
                    $text .= "\n" . bold("Chat:") . " $title (@$usernamechat)";
                } else {
                    $text .= "\n" . bold("Chat:") . " $title";
                }
                $text .= "\n" . bold("ID:") . " " . code($chatID);
                $text .= "\n" . bold("Utente:") . " " . textspecialchars("$nome $cognome") . " [" . code($userID) . "]";
                if ($username) {
                    $text .= "\n" . bold("Username:") . " @$username";
                }
                $text .= " \n" . bold("Bot:") . " @$usernameag";
                sm($config['console'], $text);
            }
            exit;
        }
        if ($idag == $userID) {
            sm($chatID, textspecialchars("$nomeag $cognomeag") . " ha abbandonato.");
        } else {
            sm($chatID,
                textspecialchars("$nomeag $cognomeag") . " è stato rimosso da " . textspecialchars("$nome $cognome"));
        }
    }

    if ($cmd == 'help') {
        $menu[0] = array(
            array(
                'text' => "Source Bot",
                'url' => 't.me/NelePHPFramework'
            ),
        );
        sm($chatID, bold("Comandi del Bot sui Gruppi") . "
start - Avvia il Bot
help - Ottieni la lista dei comandi per i Gruppi
jsondump - Ottieni un dump in json della tua update
staff - Visualizza la lista degli amministratori
dm - Elimina un messaggio
setStickers - Setta il Set Sticker del Gruppo
unsetStickers - Togli il Set Sticker del Gruppo
pin - Fissa un messaggio via reply
unpin - Togli il messaggio fissato
delpic - Togli la foto profilo del Gruppo
admin - Rendi amministratoreun utente
getchat - Prendi le info del Gruppo
membri - Guarda il numero di membri sul Gruppo
info - Informazionidi un utente via reply
setTitle - Modificail nome del Gruppo
setDescription - Modifica la descrizione del Gruppo
muta - Silenzia un utente via reply
@" . $config['username_bot'], $menu);
    }

    #Visualizza i permessi del Bot (solo per amministratori del Bot)
    if ($cmd == 'botperms' and $isadmin) {
        $emoji = array(
            0 => "❌",
            1 => "✅"
        );
        if ($botisadmin) {
            $text = "Permessi di @" . $botperms['user']['username'];
            $text .= "\n\nStato: " . code($botperms['status']);
            $active = $botperms['can_change_info'];
            $text .= "\nCambiare le info del gruppo: " . $emoji[$active];
            $active = $botperms['can_delete_messages'];
            $text .= "\nEliminare messaggi: " . $emoji[$active];
            $active = $botperms['can_restrict_members'];
            $text .= "\nBloccare utenti: " . $emoji[$active];
            $active = $botperms['can_invite_users'];
            $text .= "\nInvitare utenti tramite link: " . $emoji[$active];
            $active = $botperms['can_pin_messages'];
            $text .= "\nFissare messaggi: " . $emoji[$active];
            $active = $botperms['can_promote_members'];
            $text .= "\nAggiungere amministratori: " . $emoji[$active];
            sm($chatID, $text);
        } else {
            sm($chatID, "Nessun permesso da Admin.");
        }
    }

    #Lista Staff di un Gruppo
    if ($cmd == 'staff') {
        $config['response'] = true;
        $admis = getAdmins($chatID);
        foreach ($admis['result'] as $ad) {
            if ($ad['user']['is_bot'] !== true) {
                $nomec = $ad['user']['first_name'];
                if (isset($ad['user']['last_name'])) {
                    $nomec .= ' ' . $ad['user']['last_name'];
                }
                if (isset($ad['user']['username'])) {
                    $nomec = text_link($nomec, "t.me/" . $ad['user']['username']);
                } else {
                    $nomec = bold($nomec);
                }
                if ($ad['status'] == 'creator') {
                    $founder = $nomec;
                } else {
                    $adminis .= "\n- $nomec";
                }
            }
        }
        $config['response'] = false;
        sm($chatID, bold("Lista Amministratori \n\nCreatore:") . " $founder \n$adminis");
    }

    #Elimina messaggio via reply (Solo per gli Admin del Gruppo)
    if ($cmd == 'dm' and $isStaff) {
        if ($botisadmin) {
            if ($botperms['can_delete_messages']) {
                dm($chatID, $msgID);
                if ($reply) {
                    dm($chatID, $rmsgID);
                }
            } else {
                sm($userID, "Non ho il permesso per eliminare i messaggi.");
            }
        } else {
            sm($userID, "Non sono admin del gruppo " . bold($title) . " per cancellare i messaggi!");
        }
    }

    #Setta il Pacchetto Stickers (solo per Supergruppi con 100+ membri | Solo per gli Admin del Gruppo)
    if ($cmd == 'setStickers' and $reply and $isStaff) {
        $set = $update['message']['reply_to_message']['sticker']['set_name'];
        if ($botisadmin) {
            $r = setStickers($chatID, $set);
            if ($r['ok']) {
                sm($chatID, text_link("Set Sticker", "t.me/addstickers/$set") . " del Gruppo Settati!");
            } else {
                sm($chatID, "Impossibile modificare il Set Stickers di questo Gruppo, guarda i Logs per sapere di più");
            }
        } else {
            sm($userID, "Non sono admin del gruppo " . bold($title) . " per modificare il Set Sticker!");
        }
    }

    #Togli il Pacchetto Stickers del Gruppo (Solo per gli Admin del Gruppo)
    if ($cmd == 'unsetStickers' and $isStaff) {
        if ($botisadmin) {
            unsetStickers($chatID);
        } else {
            sm($userID, "Non sono admin del gruppo " . bold($title) . " per eliminare il Set Sticker!");
        }
    }

    #Fissa un messaggio via reply (Solo per gli Admin del Gruppo)
    if ($cmd == 'pin' and $isStaff) {
        if ($botperms['can_pin_messages']) {
            if ($reply) {
                pin($chatID, $rmsgID);
            } else {
                sm($chatID, "Rispondi a un messaggio per fissarlo", null, null, true);
            }
        } else {
            sm($userID, "Non ho il permesso di fissare messaggi su " . bold($title));
        }
    }

    #Togli il messaggio fissato (Solo per gli Admin del Gruppo)
    if ($cmd == 'unpin' and $isStaff) {
        if ($botperms['can_pin_messages']) {
            unpin($chatID);
            dm($chatID, $msgID);
        } else {
            sm($userID, "Non ho il permesso di fissare messaggi su " . bold($title));
        }
    }

    #Togli l'immagine profilo del Gruppo (Solo per gli Admin del Gruppo)
    if ($cmd == 'delpic' and $isStaff) {
        if ($botperms['can_change_info']) {
            unsetp($chatID);
            sm($chatID, "Foto Profilo di " . bold($title) . " eliminata.");
        } else {
            sm($userID, "Non ho i permessi per modificare le info in " . bold($title) . ".");
        }
    }

    #Esci da un Gruppo (Solo per gli Admin del Bot)
    if ($cmd == 'leave' and $isadmin) {
        lc($chatID);
    }

    #Rendi amministratore utente (Solo per il Creatore del Gruppo)
    if ($cmd == 'addadmin' and $isfounder) {
        if ($botperms['can_promote_members']) {
            promote($chatID, $ruserID);
        } else {
            sm($userID, "Non ho i permessi per aggiungere Amministratori in " . bold($title) . ".");
        }
    }

    #Informazioni del Gruppo in Json (Solo per gli Admin del Gruppo)
    if ($cmd == 'getchat' and $isStaff) {
        $config['response'] = true;
        $res = getChat($chatID);
        sm($chatID, 'Result: ' . code(json_encode($res, JSON_PRETTY_PRINT)));
    }

    #Ottieni il numero di membri del Gruppo
    if ($cmd == 'membri') {
        $membri = conta($chatID);
        sm($chatID, 'Membri: ' . $membri);
    }

    #Modifica il nome del Gruppo (Solo per il Creatore del Gruppo)
    if (strpos($cmd, 'setTitle') === 0 and $isfounder) {
        if ($botperms['can_change_info']) {
            $title = str_replace('setTitle ', '', $cmd);
            setTitle($chatID, $title);
        } else {
            sm($userID, "Non ho i permessi per modificare le informazioni su " . bold($title) . ".");
        }
    }

    #Modifica la descrizione del Gruppo (Solo per il Creatore del Gruppo)
    if (strpos($cmd, 'setDescription') === 0 and $isfounder) {
        if ($botperms['can_change_info']) {
            $desc = str_replace('setDescription ', '', $cmd);
            setDescription($chatID, $desc);
        } else {
            sm($userID, "Non ho i permessi per modificare le informazioni su " . bold($title) . ".");
        }
    }

    #Prendi le Informazioni di un utente (Solo per gli Admin del Gruppo)
    if ($cmd == 'info' and $isStaff and $reply) {
        $res = getChatMember($chatID, $ruserID);
        if ($res['ok']) {
            $us = $res['result']['user'];
            $status = $res['result']['status'];
            if ($us['is_bot']) {
                $type = 'Bot';
            } else {
                $type = 'Utente';
            }
            $usinfo = "ID: <code>" . $us['id'] . "</code>";
            $usinfo .= "\nNome: " . htmlspecialchars($us['first_name']);
            if (isset($us['last_name'])) {
                $usinfo .= "\nCognome: " . htmlspecialchars($us['last_name']);
            }
            if (isset($us['username'])) {
                $usinfo .= "\nUsername: @" . htmlspecialchars($us['username']);
            }
            if (isset($us['language_code'])) {
                $usinfo .= "\nLingua: " . htmlspecialchars($us['language_code']);
            }
            $stati = array(
                'member' => 'membro',
                'creator' => 'creatore',
                'kicked' => 'bandito',
                'administrator' => 'amministratore',
                'left' => 'fuori dal gruppo'
            );
            $stato = $stati[$status];
            sm($chatID, bold("$type: $stato") . "\n$usinfo");
        }
    }

    #Silenzia un utente (Solo per gli Admin del Gruppo)
    if ($cmd == 'muta' and $isStaff) {
        if ($botperms['can_restrict_members']) {
            limita($chatID, $ruserID);
            sm($chatID, "$rnome è stato mutato!");
        } else {
            sm($userID, "Non ho i permessi per mutare un utente su " . bold($title) . ".");
        }
    }

    #Fine comandi per gruppi
}