# Versione: <code>2.5</code>

<b>Changelog:</b>
- ➕🔥 Aggiunto il plugin per la gestione di utenti, gruppi e canali (<code>gestione.php</code>)
- ➕🔥 Aggiunte 2 nuove tables per i database (gruppi e canali), necessarie per usare il plugin di gestione
- ➕🔥 Aggiunta la possibilità di inviare post globali (usando <code>/post</code>)
- ➕🔥 Aggiunta la possibilità di bannare dei gruppi e canali dall'uso del bot
- ➕ Aggiunto il comando <code>/query</code> per eseguire delle query PDO manualmente sui database in uso
- ➕ Rinnovato il comando <code>/performance</code>, da ora invierà in risposta il tempo di elaborazione del Framework per ogni risposta
- ➕ Aggiunto il comando <code>/ping</code> che darà in risposta l'output del "vecchio" /performance
- ➕ Aggiunte nuove variabili per le updates di Telegram
- ➕ Da ora i comandi <code>/redis</code> e <code>/database</code> funzioneranno ugualmente anche se l'uso di redis e/o database è disattivato dal config
- ✅ Fixato il ban degli utenti
- ✅ Fix al Plugin Manager<br>

<b>Buon divertimento con il Framework!</b> <br/> © [PHP-Coders](https://gitlab.com/PHP-Coders)